import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import forum from './views/asembliya.vue'
import landingPage from './views/About.vue'
// import SMS from './components/smsdata.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'landingPage',
      component: landingPage
    },
    {
      path: '/dashboard',
      name: 'home',
      component: Home
    },
    {
      path: '/asembliya',
      name: 'forum',
      component: forum
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
